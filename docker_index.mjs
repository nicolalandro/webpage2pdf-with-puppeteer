import puppeteer from 'puppeteer'


const browser = await puppeteer.launch({
    headless: true,
    executablePath: '/usr/bin/google-chrome',
    args: ['--no-sandbox']
});
const page = await browser.newPage();
await page.goto("https://news.ycombinator.com/", {
    waitUntil: "networkidle2"
});
await page.setViewport({ width: 1680, height: 1050 });
await page.pdf({
    path: "out/hacker_news.pdf",
    format: "A4"
});
console.log('pippo!')
await browser.close();